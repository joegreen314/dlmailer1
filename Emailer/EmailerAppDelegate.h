//
//  EmailerAppDelegate.h
//  Emailer
//
//  Created by Joe Green on 4/17/13.
//  Copyright (c) 2013 Digilog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailerAppDelegate : UIResponder <UIApplicationDelegate> {
    UIPopoverController *createPatientPopover;
}

@property (strong, nonatomic) UIWindow *window;

@end
